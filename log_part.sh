#!/bin/bash

tar -C ~/894065-linux-access-log-cleanup -xvf ~/894065-linux-access-log-cleanup/archive.tar.gz

for file in $(ls access.0*.log);
  do cat $file | grep -Ew '((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])'; done > /tmp/access.log